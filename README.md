# Cell Manager #

This repository contains the CloudLightning Cell Manager. Cell Manager is used for dispatching resource requests to the appropriate VRackManagerGroup. 

## Important notice: ##
* Until the VRackManager components will be ready, this component will print the messages received on the queue to standard output, presenting also which of the CellControllers received the message.


### How do I get set up? ###

#### Requirements: ####
* Maven
* [CloudLightning Data Model](https://bitbucket.org/cloudlightning/cloudlightning-data-model) artifact installed.

#### Usage: ####
1. Change into the root directory and execute `mvn clean package`. 
2. Execute the jar packaged in the `target` folder to start listening on the `Resource_Request_Queue`. 

#### Testing the program ####
* [This repository](https://bitbucket.org/adispataru/cell-manager-tester) can be used to test the functionality of the Cell Manager. 

### Who do I talk to? ###

* Adrian Spataru (adrian.spataru@e-uvt.ro)