package eu.cloudlightning.sosm.cellmanager.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import eu.cloudlightning.core.datamodel.request.ResourceTemplate;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by adispataru on 13-Jun-16.
 */
public class CellManagerConsumer extends DefaultConsumer {

    private final AtomicLong lastMessage;
    private final long id;

    public CellManagerConsumer(Channel channel, long id){
        super(channel);
        this.id = id;
        this.lastMessage = new AtomicLong(System.currentTimeMillis());
    }

    @Override
    public void handleConsumeOk(String s) {

    }

    @Override
    public void handleCancelOk(String s) {

    }

    @Override
    public void handleCancel(String s) throws IOException {

    }

    @Override
    public void handleShutdownSignal(String s, ShutdownSignalException e) {

    }

    @Override
    public void handleRecoverOk(String s) {

    }

    @Override
    public void handleDelivery(String s, Envelope envelope, AMQP.BasicProperties basicProperties, byte[] body) throws IOException {
        String message = new String(body, "UTF-8");
        ObjectMapper objectMapper = new ObjectMapper();
        ResourceTemplate rt = objectMapper.readValue(message, ResourceTemplate.class);
        System.out.println(" [" + id + "] Received '" + rt.getBpId() + "'");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lastMessage.set(System.currentTimeMillis());

        super.getChannel().basicAck(envelope.getDeliveryTag(), false);
    }

    public AtomicLong getLastMessage() {
        return lastMessage;
    }
}
