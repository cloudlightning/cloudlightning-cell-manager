package eu.cloudlightning.sosm.cellmanager.controllers;

import com.rabbitmq.client.*;
import eu.cloudlightning.sosm.cellmanager.components.ResourceDiscovery;
import eu.cloudlightning.sosm.cellmanager.components.ResourceSelection;
import eu.cloudlightning.sosm.cellmanager.resources.HardwareType;
import eu.cloudlightning.sosm.cellmanager.resources.VRMGResource;
import eu.cloudlightning.sosm.cellmanager.session.BPSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

/**
 * Created by adispataru on 08-Jun-16.
 */
public class CellManagerController implements Runnable{
    private static final Logger LOG = Logger.getLogger("CellManagerController");

    private static final String queueName = ResourceRequestQueueMonitor.QUEUE_NAME;


    protected ResourceSelection resourceSelection;
    protected ResourceDiscovery resourceDiscovery;
    protected Map<String, BPSession> bpSessionCache;
    protected List<VRMGResource> vrmgResourceCache;
    private long timeout = 5000;
    private final Connection connection;
    public static AtomicLong counter;
    private final Long id;

    public CellManagerController(Connection connection){
        if(counter == null){
            counter = new AtomicLong(0);
        }
        this.id = counter.incrementAndGet();
        LOG.info("Created CellController number " + id);
        this.connection = connection;

    }

    @Override
    public void run() {

        try {
            final Channel channel = connection.createChannel();
            channel.queueDeclare(queueName, true, false, false, null);
            channel.basicQos(1);

            System.out.println(" [" + id + "] Waiting for messages.");
            CellManagerConsumer consumer = new CellManagerConsumer(channel, id);

            String tag = channel.basicConsume(queueName, false, consumer);
            while (true){
                long now = System.currentTimeMillis();
                long lastMessageReceived = consumer.getLastMessage().get();
                if(now - lastMessageReceived > timeout &&
                        counter.get() > 1) {
                    LOG.info("[" + id + "] Nothing received in " + (now - lastMessageReceived) + "ms, dissolving cellController..");
                    channel.basicCancel(tag);
                    break;
                }
                else{
                    if(counter.get() > 1){
                        LOG.info("[" + id + "] is receiving.. Keep alive for 5000 ms");
                    }else{
                        LOG.info("[" + id + "] is the main controller.. Keep alive for 5000 ms");
                    }
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            counter.decrementAndGet();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }


    public boolean updateVRMGResourceCache(String operation){
        return false;
    }

    public boolean persistVRMGResourceCache(){
        return false;
    }

    public void refreshVRMGResourceCache(){
        vrmgResourceCache = new ArrayList<VRMGResource>();
        Random r = new Random(System.currentTimeMillis());
        for(int i = 0; i < 10; i++){
            VRMGResource res = new VRMGResource();
            res.setCpuCapacity(r.nextDouble());
            res.setEstimatedPower(r.nextDouble());
            res.setMemoryCapacity(r.nextInt(32));
            res.setHardwareType(HardwareType.CPU);
            res.setNetBWCapacity(r.nextInt(1024));
            res.setStorageCapacity(r.nextInt(2048));
            res.setVrmgId(String.valueOf(i));
            vrmgResourceCache.add(res);
        }

    }

    public boolean receiveBPRequest(){
        return false;
    }

    public void cacheBPSession(){

    }

    public boolean sendBPResource(){
        return false;
    }

}
