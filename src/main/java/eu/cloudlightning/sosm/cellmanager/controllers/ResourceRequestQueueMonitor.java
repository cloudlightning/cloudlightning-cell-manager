package eu.cloudlightning.sosm.cellmanager.controllers;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

/**
 * Created by adispataru on 13-Jun-16.
 */
public class ResourceRequestQueueMonitor {

    public static final String QUEUE_NAME = "Resource_request_queue";
    public static final String QUEUE_HOST = "localhost";
    private static final Logger LOG = Logger.getLogger(ResourceRequestQueueMonitor.class.getName());
    private ConnectionFactory connectionFactory;

    public ResourceRequestQueueMonitor(){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(QUEUE_HOST);
        this.connectionFactory = factory;
    }


    public ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    public Connection createConnection() throws IOException, TimeoutException {
        return connectionFactory.newConnection();
    }
}
