package eu.cloudlightning.sosm.cellmanager.components;

import eu.cloudlightning.sosm.cellmanager.strategies.CostStrategy;
import eu.cloudlightning.sosm.cellmanager.strategies.CostStrategyContext;

import java.util.logging.Logger;

/**
 * Created by adispataru on 08-Jun-16.
 */
public class ResourceSelection {
    private static final Logger LOG = Logger.getLogger("Resource Selection");
    private CostStrategyContext costStrategyContext;

    public void selectResource(String strategy){
        CostStrategy costStrategy = costStrategyContext.createCostStrategy(strategy);
        if(costStrategy != null){
            costStrategy.optimize();

        }

    }

    public String calculateBPSolution(){
        String solution = "";

        solution += "not/implemented";
        return solution;
    }

    public boolean verifySolution(){
        return false;
    }
}
