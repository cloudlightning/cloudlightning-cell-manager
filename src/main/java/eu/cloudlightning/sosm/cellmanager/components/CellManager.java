package eu.cloudlightning.sosm.cellmanager.components;

import eu.cloudlightning.sosm.cellmanager.controllers.CellManagerController;

/**
 * Created by adispataru on 08-Jun-16.
 */
public class CellManager {
    private String cellManagerId;
    private String cellManagerDomain;
    private String cellManagerDescription;

    public CellManager(String id){
        this.cellManagerId = id;
    }

    public CellManager(String cellId, String cellDomain){
        this.cellManagerId = cellId;
        this.cellManagerDomain = cellDomain;
    }

    public CellManager(String cellId, String cellDomain, String cellManagerDescription){
        this.cellManagerId = cellId;
        this.cellManagerDomain = cellDomain;
        this.cellManagerDescription = cellManagerDescription;
    }

    public String getCellManagerId() {
        return cellManagerId;
    }

    public void setCellManagerId(String cellManagerId) {
        this.cellManagerId = cellManagerId;
    }

    public String getCellManagerDomain() {
        return cellManagerDomain;
    }

    public void setCellManagerDomain(String cellManagerDomain) {
        this.cellManagerDomain = cellManagerDomain;
    }

    public String getCellManagerDescription() {
        return cellManagerDescription;
    }

    public void setCellManagerDescription(String cellManagerDescription) {
        this.cellManagerDescription = cellManagerDescription;
    }

}
