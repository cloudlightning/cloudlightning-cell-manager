package eu.cloudlightning.sosm.cellmanager.session;

/**
 * Created by adispataru on 08-Jun-16.
 */
public class BPSession {
    private String bpId;
    private String resourceTemplate;

    public String getBpId() {
        return bpId;
    }

    public void setBpId(String bpId) {
        this.bpId = bpId;
    }

    public String getResourceTemplate() {
        return resourceTemplate;
    }

    public void setResourceTemplate(String resourceTemplate) {
        this.resourceTemplate = resourceTemplate;
    }
}
