package eu.cloudlightning.sosm.cellmanager.strategies;

/**
 * Created by adispataru on 08-Jun-16.
 */
public class CostStrategyContext {

    public CostStrategy createCostStrategy(String strategy){
        if(CostStrategies.MultiObjective.toString().equals(strategy)){
            return new MultiObjectiveCostStrategy();
        }
        else if(CostStrategies.Performance.toString().equals(strategy)){
            return new PerformanceCostStrategy();
        }
        else if(CostStrategies.Power.toString().equals(strategy)){
            return new PowerCostStrategy();
        }
        else if(CostStrategies.Value.toString().equals(strategy)){
            return new ValueCostStrategy();
        }
        return null;
    }


}
