package eu.cloudlightning.sosm.cellmanager.strategies;

/**
 * Created by adispataru on 08-Jun-16.
 */
public enum CostStrategies {
    MultiObjective ("MultiObj"),
    Power ("Power"),
    Performance ("Performance"),
    Value ("Value");
    private final String name;

    private CostStrategies(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return otherName != null && name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
