package eu.cloudlightning.sosm.cellmanager.strategies;

/**
 * Created by adispataru on 08-Jun-16.
 */
public interface CostStrategy {

    void optimize();
}
