package eu.cloudlightning.sosm.cellmanager;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.tools.Tracer;
import eu.cloudlightning.sosm.cellmanager.controllers.CellManagerConsumer;
import eu.cloudlightning.sosm.cellmanager.controllers.CellManagerController;
import eu.cloudlightning.sosm.cellmanager.controllers.ResourceRequestQueueMonitor;
import eu.cloudlightning.sosm.cellmanager.rest.RabbitMQAdminClient;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

/**
 * Created by adispataru on 08-Jun-16.
 */
public class Application {

    private static final Logger LOG = Logger.getLogger("Application");
    private static final int MAX_THREADS = 10;

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        LOG.info("Application started.");
        ResourceRequestQueueMonitor rqm = new ResourceRequestQueueMonitor();
        RabbitMQAdminClient client = new RabbitMQAdminClient();

        ExecutorService service = Executors.newFixedThreadPool(MAX_THREADS);

         /** Be aware that the same connection is used for all the channels */
        Connection connection = rqm.createConnection();

        CellManagerController mainController = new CellManagerController(connection);
        service.execute(mainController);

        while (true) {
            String queueStatus =  client.getQueueStatus();

            JsonNode rootQueue = new ObjectMapper().readTree(new StringReader(queueStatus));
            JsonNode messages = rootQueue.get("messages");

            long messCount = messages.asInt();
            long controllerCount = CellManagerController.counter.get();

            long ratio = messCount / controllerCount;

            if(ratio > 20 && controllerCount < MAX_THREADS){

                CellManagerController controller = new CellManagerController(connection);

                service.execute(controller);
//                service.execute(controller1);
            }
            Thread.sleep(1000);

        }

    }
}
