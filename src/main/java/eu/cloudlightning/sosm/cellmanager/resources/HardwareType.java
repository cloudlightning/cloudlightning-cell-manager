package eu.cloudlightning.sosm.cellmanager.resources;

/**
 * Created by adispataru on 08-Jun-16.
 */
public enum HardwareType {
    CPU,
    GPU,
    MIC,
    DFE,
    NUMA
}
