package eu.cloudlightning.sosm.cellmanager.resources;

import java.util.List;

/**
 * Created by adispataru on 08-Jun-16.
 */
public class VRMGResource {
    private String vrmgId;
    private HardwareType hardwareType;
    private double cpuCapacity;
    private int memoryCapacity;
    private int storageCapacity;
    private int netBWCapacity;
    private List<String> coalitionList;
    private double estimatedPower;

    public String getVrmgId() {
        return vrmgId;
    }

    public void setVrmgId(String vrmgId) {
        this.vrmgId = vrmgId;
    }

    public HardwareType getHardwareType() {
        return hardwareType;
    }

    public void setHardwareType(HardwareType hardwareType) {
        this.hardwareType = hardwareType;
    }

    public double getCpuCapacity() {
        return cpuCapacity;
    }

    public void setCpuCapacity(double cpuCapacity) {
        this.cpuCapacity = cpuCapacity;
    }

    public int getMemoryCapacity() {
        return memoryCapacity;
    }

    public void setMemoryCapacity(int memoryCapacity) {
        this.memoryCapacity = memoryCapacity;
    }

    public int getStorageCapacity() {
        return storageCapacity;
    }

    public void setStorageCapacity(int storageCapacity) {
        this.storageCapacity = storageCapacity;
    }

    public int getNetBWCapacity() {
        return netBWCapacity;
    }

    public void setNetBWCapacity(int netBWCapacity) {
        this.netBWCapacity = netBWCapacity;
    }

    public List<String> getCoalitionList() {
        return coalitionList;
    }

    public void setCoalitionList(List<String> coalitionList) {
        this.coalitionList = coalitionList;
    }

    public double getEstimatedPower() {
        return estimatedPower;
    }

    public void setEstimatedPower(double estimatedPower) {
        this.estimatedPower = estimatedPower;
    }

    @Override
    public String toString(){
        return String.format("[id = %s; hardware = %s; cpu = %s; memory = %d; storage = %d; netBW = %d; estPower = %s]",
                vrmgId, hardwareType, cpuCapacity, memoryCapacity, storageCapacity, netBWCapacity, estimatedPower);
    }
}
