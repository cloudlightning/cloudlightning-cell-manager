package eu.cloudlightning.sosm.cellmanager.rest;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

/**
 * Created by adispataru on 14-Jun-16.
 */
public class RabbitMQAdminClient {
    private static final String user = "guest";
    private static final String password = "guest";

    public String getQueueStatus(){
        Client client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter(user, password));

        WebResource webResource = client
                .resource("http://localhost:15672/api/queues/%2F/Resource_request_queue");

        ClientResponse response = webResource.accept("application/json")
                .get(ClientResponse.class);

        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        return response.getEntity(String.class);

    }
}
